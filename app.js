// app.js
// App({
//   onLaunch() {// app.js
App({
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },

  // 合并两个 globalData 对象的内容
  globalData: {
    userInfo: null,
    wxUserInfo: null
  }
})

// 定义全局变量 courseFileUrl
// const courseFileUrl = '/course.txt';

// 将 openCourseFile 方法作为 App 实例的成员方法
// App.prototype.openCourseFile = function() {
//   wx.openDocument({
//     filePath: courseFileUrl,
//     success: function(res) {
//       console.log('打开文件成功');
//     },
//     fail: function(err) {
//       console.error('打开文件失败', err);
//     }
//   });
// }

//     // 展示本地存储能力
//     const logs = wx.getStorageSync('logs') || []
//     logs.unshift(Date.now())
//     wx.setStorageSync('logs', logs)

//     // 登录
//     wx.login({
//       success: res => {
//         // 发送 res.code 到后台换取 openId, sessionKey, unionId
//       }
//     })
//   },
//   globalData: {
//     userInfo: null
//   }
// })

// App({
//   onLaunch() {
//   },
//   globalData:{
//     wxUserInfo:null
//   }
// })

// const courseFileUrl = '/course.txt';

// // 将 openCourseFile 方法作为 App 实例的成员方法
// App.prototype.openCourseFile = function() {
//   wx.openDocument({
//     filePath: courseFileUrl,
//     success: function(res) {
//       console.log('打开文件成功');
//     },
//     fail: function(err) {
//       console.error('打开文件失败', err);
//     }
//   });
// }

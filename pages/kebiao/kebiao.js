// pages/kebiao/kebiao.js
Page({

  /**
   * 页面的初始数据
   *  wList: [   //id：代表颜色，id不同，颜色不同
              //isToday：代表星期几，数字是几就是周几
              //classNumber：代表占几个课时，也就是时间长度占几个最小长方形
              //name：课程名字
              //address：教室的位置和号码
      [     //第一周 
        { "id":1,"isToday": 1, "jie": 5, "classNumber": 2, "name": "算法设计与分析","address":"2306" },
        { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
        { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },

        { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
        { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
        {"id":7,"isToday": 2, "jie": 7, "classNumber": 2, "name": "数学建模","address":"1215"},
       
        { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
        { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },

        { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
        { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "计算机网络" ,"address":"2304" },
        
        { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
   ],
      [      //第二周
  
        { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
        { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },

        { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
        { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
        {"id":7,"isToday": 2, "jie": 7, "classNumber": 2, "name": "数学建模","address":"3301"},
        { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
        { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },

        { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
        { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"2306" },
        
        { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },],
        [     //第三周 
          { "id":1,"isToday": 1, "jie": 7, "classNumber": 2, "name": "算法设计与分析","address":"2306" },
          { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
          { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
  
          { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
          { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
          {"id":7,"isToday": 2, "jie": 7, "classNumber": 2, "name": "数学建模","address":"未知"},
          { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
          { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
  
          { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
          { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "计算机网络" ,"address":"2304" },
          
          { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
     ],
        [      //第四周
    
          { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
          { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
          {"id":7,"isToday": 2, "jie": 7, "classNumber": 2, "name": "数学建模","address":"未知"},
          { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
          { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
         
          { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
          { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
  
          { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
          { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"2306" },
          
          { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
        ],
          [     //第五周 
            { "id":1,"isToday": 1, "jie": 7, "classNumber": 2, "name": "算法设计与分析","address":"2306" },
            {"id":7,"isToday": 3, "jie": 1, "classNumber": 2, "name": "数学建模","address":"未知"},
            { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
            { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
    
            { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
            { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
           
            { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
            { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
    
            { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
            { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "计算机网络" ,"address":"2304" },
            
            { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
       ],
          [      //第六周
      
            { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
            { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
            {"id":7,"isToday": 3, "jie": 1, "classNumber": 2, "name": "数学建模","address":"未知"},
            { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
            { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
           
            { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
            { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
    
            { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
            { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"2306" },
            
            { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
          ],
            [     //第七周 
              { "id":1,"isToday": 1, "jie": 7, "classNumber": 2, "name": "算法设计与分析","address":"2306" },
              { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
              { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
              {"id":7,"isToday": 3, "jie": 1, "classNumber": 2, "name": "数学建模","address":"未知"},
              { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
              { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
             
              { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
              { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
      
              { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
              { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "计算机网络" ,"address":"2304" },
              
              { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
         ],
            [      //第八周
        
              { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
              { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
              {"id":7,"isToday": 3, "jie": 1, "classNumber": 2, "name": "数学建模","address":"未知"},
              { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
              { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
             
              { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
              { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
      
              { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
              { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"2306" },
              
              { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
            ],
              [     //第九周 
                { "id":1,"isToday": 1, "jie": 7, "classNumber": 2, "name": "算法设计与分析","address":"2306" },
                { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                {"id":7,"isToday": 5, "jie": 1, "classNumber": 2, "name": "数学建模","address":"未知"},
                { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
               
                { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
        
                { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "计算机网络" ,"address":"2304" },
                
                { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
           ],
              [      //第十周
          
                { "id":1,"isToday": 1, "jie": 1, "classNumber": 2, "name": "马克思主义原理概论" ,"address":"3107" },
                { "id":2,"isToday": 1, "jie": 3, "classNumber": 2, "name": "功能高分子材料","address":"3304" },
                { "id":3,"isToday": 1, "jie": 6, "classNumber": 2, "name": "光固化技术及应用","address":"2318" },
                { "id":4,"isToday": 1, "jie": 8, "classNumber": 2, "name": "工程伦理","address":"2320" },

                {"id":7,"isToday": 5, "jie": 1, "classNumber": 2, "name": "数学建模","address":"未知"},
                { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
               
                { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
        
                { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"2306" },
                
                { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
              ],
                [     //第十一周 
                  { "id":1,"isToday": 1, "jie": 3, "classNumber": 2, "name": "Python计算科学基础","address":"教1-514B" },
                  { "id":2,"isToday": 1, "jie": 6, "classNumber": 2, "name": "实验设计与数据处理" ,"address":"2316" },
                  { "id":3,"isToday": 2, "jie": 3, "classNumber": 2, "name": "高分子材料","address":"2202" },
                  {"id":2,"isToday": 3, "jie": 1, "classNumber": 2, "name": "实验设计与数据处理","address":"2418"},
                  { "id":4,"isToday": 3, "jie": 3, "classNumber": 2, "name": "工程伦理" ,"address":"2320" },
                  { "id":5,"isToday": 4, "jie": 3, "classNumber": 2, "name": "马克思主义基本原理" ,"address":"3109" },
                 
                  { "id":6,"isToday": 5, "jie": 3, "classNumber": 2, "name": "生物质能利用技术" ,"address":"1337" },
                   { "id":7,"isToday": 6, "jie": 6, "classNumber": 4, "name": "计算机网络" ,"address":"5107" },
          
                   { "id":8,"isToday": 7, "jie": 1, "classNumber": 4, "name": "数据库系统" ,"address":"5107" },
                   { "id":9,"isToday": 7, "jie": 6, "classNumber": 4, "name": "软件工程" ,"address":"5103" },
                  

             ],
                [      //第十二周
            
                  { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                  { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                  {"id":7,"isToday": 5, "jie": 1, "classNumber": 2, "name": "数学建模","address":"未知"},
                  { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                  { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
                 
                  { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                  { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
          
                  { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                  { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"2306" },
                  
                  { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
                ],
                  [     //第十三周 
                    { "id":1,"isToday": 1, "jie": 7, "classNumber": 2, "name": "算法设计与分析","address":"2306" },
                    { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                    { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                    {"id":7,"isToday": 3, "jie": 5, "classNumber": 2, "name": "数学建模","address":"未知"},
                    { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                    { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
                   
                    { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                    { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
            
                    { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                    { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "计算机网络" ,"address":"2304" },
                    
                    { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
               ],
                  [      //第十四周
              
                    { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                    { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                    {"id":7,"isToday": 3, "jie": 5, "classNumber": 2, "name": "数学建模","address":"未知"},
                    { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                    { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
                   
                    { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                    { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
            
                    { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                    { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"2306" },
                    
                    { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
                  ],
                    [     //第十五周 
                      { "id":1,"isToday": 1, "jie": 7, "classNumber": 2, "name": "算法设计与分析","address":"2306" },
                      { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                      { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                      {"id":7,"isToday": 3, "jie": 5, "classNumber": 2, "name": "数学建模","address":"未知"},
                      { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                      { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
                     
                      { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                      { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
              
                      { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                      { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "计算机网络" ,"address":"2304" },
                      
                      { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
                 ],
                    [      //第十六周
                
                      { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                      { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                      {"id":7,"isToday": 3, "jie": 5, "classNumber": 2, "name": "数学建模","address":"未知"},
                      { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                      { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
                     
                      { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                      { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
              
                      { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                      { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"2306" },
                      
                      { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
                    ],
                      [     //第十七周 
                        
                        { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                        { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                        { "id":1,"isToday": 1, "jie": 7, "classNumber": 2, "name": "算法设计与分析","address":"2306" },
                
                        { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                        { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
                       
                        { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                        { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                
                        { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                        { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "计算机网络" ,"address":"2304" },
                        
                        { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
                   ],
                      [      //第十八周
                  
                        { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                        { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                
                        { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                        { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
                       
                        { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                        { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                
                        { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                        { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"2306" },
                        
                        { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
                      ],
                        [     //第十九周 
                          { "id":1,"isToday": 1, "jie": 7, "classNumber": 2, "name": "算法设计与分析","address":"2306" },
                          { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                          { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                  
                          { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                          { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
                         
                          { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                          { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                  
                          { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                          { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "计算机网络" ,"address":"2304" },
                          
                          { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
                     ],
                        [      //第二十周
                    
                          { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                          { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                  
                          { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                          { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
                         
                          { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                          { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                  
                          { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                          { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"2306" },
                          
                          { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
                        ],
                          [     //第二十一周 
                            { "id":1,"isToday": 1, "jie": 7, "classNumber": 2, "name": "算法设计与分析","address":"2306" },
                            { "id":2,"isToday": 1, "jie": 1, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                            { "id":3,"isToday": 1, "jie": 3, "classNumber": 2, "name": "毛概","address":"6202" },
                    
                            { "id":4,"isToday": 2, "jie": 3, "classNumber": 2, "name": "Matlab" ,"address":"2306" },
                            { "id":5,"isToday": 2, "jie": 5, "classNumber": 2, "name": "数据库原理及应用" ,"address":"1104" },
                           
                            { "id":6,"isToday": 3, "jie": 3, "classNumber": 2, "name": "计算机网络" ,"address":"5102" },
                            { "id":2,"isToday": 3, "jie": 7, "classNumber": 2, "name": "操作系统" ,"address":"5409" },
                    
                            { "id":3,"isToday": 4, "jie": 1, "classNumber": 2, "name": "毛概" ,"address":"6202" },
                            { "id":6,"isToday": 4, "jie": 5, "classNumber": 2, "name": "计算机网络" ,"address":"2304" },
                            
                            { "id":1,"isToday": 5, "jie": 3, "classNumber": 2, "name": "算法设计与分析" ,"address":"5506" },
                       ],
    // otherCourse:[{"id":1,"name":"应用软件开发课程设计","time":"18-19周","address":"无","teacher":"沈华"},]
  ]},
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
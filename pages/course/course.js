var colors = require('../../utils/colors.js');
const app = getApp();
var localdata = require('../../data/course.js');
Page({

  /**
   * 页面的初始数据
   */
  data:{
    weekArray: ['第1周', '第2周', '第3周', '第4周', '第5周', '第6周', '第7周', '第8周', '第9周', '第10周', '第11周', '第12周', '第13周', '第14周', '第15周', '第16周', '第17周', '第18周', '第19周', '第20周', '第21周'],
    pageNum: 0, // 当前所在分类的索引
    todayDay: '', // 今日日期
    todayMonth:'', // 今日月份
    todayWeek:'', // 今日周
    day:'', // 日期
    month: '', // 月份
    monthNum:1,
    week: ['一', '二', '三', '四', '五', '六','日'], // 周一为起始日
    nowDay:[1,2,3,4,5,6,7], // 本周的七天日期
    schoolTime: ['2023','02','20'], // 本学期开学时间
    nowWeek: '', // 当前周
    course_time:[
      ['8:30','9:15'],
      ['9:20','10:05'],
      ['10:25','11:10'],
      ['11:15','12:00'],
      ['13:50','14:35'],
      ['14:40','15:25'],
      ['15:30','16:15'],
      ['16:30','17:15'],
      ['17:20','18:05'],
      ['18:30','19:15'],
      ['19:20','20:05'],
      ['20:10','20:55'],
    ],
    courses:[],
    weekcourse : [],
  },
    parseperiods(periods) {
  // 解析 periods 字符串
    // console.log(`periods: ${periods}`);
    var periodsstr=periods.toString()//??????
    // console.log(`periodsstr: ${periodsstr}`);
    const periodNumbers = periodsstr.split(',');
    // console.log(`periodNumbers: ${periodNumbers}`);
  // 存储该课程所有上课时间段的集合
    const coursePeriods = [];

  // 遍历 periodNumbers，获取对应的上课时间范围
    for (const periodNum of periodNumbers) {
      // console.log(`periodsNum: ${periodNum}`);
      const index = parseInt(periodNum) - 1; // 转换为数组索引（从0开始）
      // console.log(`index: ${index}`);
      // console.log(`parseInt(periodNum)-1: ${parseInt(periodNum)-1}`);
      // console.log(`typeof(parseInt(periodNum)-1): ${typeof(parseInt(periodNum)-1)}`);
      if (index >= 0 && index < this.data.course_time.length) {
        const timeRange = this.data.course_time[index];
        // console.log(`timeRange: ${timeRange}`);
        coursePeriods.push(timeRange);
        // console.log(`coursePeriods: ${coursePeriods}`);
      } else {
        // console.error(`Invalid index: ${index}. It should be within [0, ${this.data.course_time.length - 1}]`);
      };
      // console.log(`this.data.course_time[index]: ${this.data.course_time[index]}`);
    };
    function parseTimeToMinutes(timeStr) {
      const [hours, minutes] = timeStr.split(':').map(Number);
      return hours * 60 + minutes;
    };

    let totalDurationInMinutes = 0;
    let earliestStartTime = null;
    var startTimeStr ='';

    for (let i =0;i < coursePeriods.length;i++) {
      earliestStartTime = parseTimeToMinutes(coursePeriods[0][0]);
      startTimeStr = coursePeriods[i][0];
      const endTimeStr = coursePeriods[i][1];
      // console.log(`startTimeStr: ${startTimeStr}`);
      // console.log(`endTimeStr: ${endTimeStr}`);
      const startTime = parseTimeToMinutes(startTimeStr);
      // console.log(`startTime: ${startTime}`);
      const endTime = parseTimeToMinutes(endTimeStr);
      // console.log(`endTime: ${endTime}`);
      const durationInMinutes = endTime - startTime;
      // console.log(`durationInMinutes: ${durationInMinutes}`);
      totalDurationInMinutes += durationInMinutes;
      // console.log(`totalDurationInMinutes: ${totalDurationInMinutes}`);
    };

      // 记录最早的开始时间
      // if (earliestStartTime === null || startTime < parseTimeToMinutes(earliestStartTime)) {
      //   earliestStartTime = coursePeriods[0][0];
      //   console.log(`earliestStartTime: ${earliestStartTime}`);
      // }


    // 计算得到的总时长（单位：分钟）
    const totalDuration = totalDurationInMinutes;

    // 课程开始上课的最早时间
    const earliestStartTimeFormatted = earliestStartTime;

    // console.log('课程总时长:', totalDuration);
    // console.log('课程最早开始时间:', earliestStartTimeFormatted);
    return {
      totalDuration,
      earliestStartTimeFormatted,
  };
},
setweekcourse(nowWeek){                       
  console.log(`nowWeek:${nowWeek}`);
  let weekcourse = [];
  //console.log(`weekcourse:`, JSON.stringify(weekcourse, null, 2)); 
  //console.log(`localdata.localdata.length: ${localdata.localdata.length}`);
  for(let i=0;i<localdata.localdata.length;i++){
    if(localdata.localdata[i].week == nowWeek){
      // console.log(`${localdata.localdata[i]}: `,JSON.stringify(localdata.localdata[i], null, 2));
      weekcourse.push(localdata.localdata[i]);
      // console.log(`weekcourse:`, JSON.stringify(this.data.weekcourse, null, 2)); 
    //return weekcourse;
  };
  this.setData({
    weekcourse:weekcourse,
  });
  };
  console.log(`this.data.weekcourse.length: ${this.data.weekcourse.length}`);
  for(let i=0;i<this.data.weekcourse.length;i++){
    console.log(i, JSON.stringify(this.data.weekcourse[i], null, 2));
  };
  return this.data.weekcourse;
},

  onLoad(options) {
    let nowWeek = this.getNowWeek();
    console.log(`nowWeek:${nowWeek}`);
    let nowDay = this.getDayOfWeek(nowWeek);
    let pageNum = nowWeek;
    let month = this.getMonth((nowWeek ) * 7);
    let courses = localdata.localdata;
    let weekcourse = this.setweekcourse(nowWeek);
    console.log(`weekcourse:`, JSON.stringify(weekcourse, null, 2)); // 使用JSON.stringify美化输出
    // 对每门课程应用 parseperiods 函数
    let parsedCourses = courses.map((courses) => {
      const { totalDuration, earliestStartTimeFormatted } = this.parseperiods(courses.periods);
      return {
        ...courses,
        // ...this.parseperiods(courses),
        totalDuration,
        earliestStartTimeFormatted,
      };
    });
    const firstCourseTotalDuration = parsedCourses[0].totalDuration;
    // console.log(`firstCourseTotalDuration: ${firstCourseTotalDuration}`);
    const firstCourseEarliestStartTimeFormatted = parsedCourses[0].earliestStartTimeFormatted;
    // console.log(`firstCourseEarliestStartTimeFormatted: ${firstCourseEarliestStartTimeFormatted}`);
    // 使用已计算出的最早开始时间和总时长
    // let topmargin = earliestStartTimeFormatted;
    // let height = totalDuration;
    this.data.todayMonth;
    this.setData({
      nowWeek,
      nowDay,
      pageNum,
      todayWeek: nowWeek,
      monthNum: month / 1, // 当前月份数字类型，用于数字运算
      colorArrays: colors, // 课表颜色
      courses:parsedCourses,
      totalDuration: firstCourseTotalDuration,
      earliestStartTimeFormatted: firstCourseEarliestStartTimeFormatted,
      weekcourse,
    });
  },

  // 获取第几周后的月份
  getMonth(days) {
    let [year, month, day] = this.data.schoolTime;
    var date = new Date(year, month - 1, day);
    date.setDate(date.getDate() + days); // 获取n天后的日期
    var m = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
    return m;
  },

  // 获取第几周后的星期几的日期
  getDay(days) {
    let [year, month, day] = this.data.schoolTime;
    var date = new Date(year, month - 1, day);
    date.setDate(date.getDate() + days); // 获取n天后的日期
    var d = date.getDate() < 10 ? "0" + date.getDate() : date.getDate(); // 获取当前几号，不足10补0
    return d;
  },

  // 获取当前周
  getNowWeek() {
    var date = new Date();
    let [year, month, day] = this.data.schoolTime;
    var start = new Date(year, month - 1, day);
    // 计算时间差
    var left_time = parseInt((date.getTime() - start.getTime()) / 1000) + 24 * 60 * 60;
    var days = parseInt(left_time / 3600 / 24);
    var week = Math.floor(days / 7);
    var result = week;
    if (week > 20 || week <= 0) {
      result = this.data.now_week;
    }
    return result;
  },

  // 获取一周的日期
  getDayOfWeek(week) {
    var day = [];
    for (var i = 0; i < 7; i++) {
      var days = (week - 1) * 7 + i;
      day.push(this.getDay(days));
    }
    return day;
  },

  // 获取课表数据
  async getCourseList() {},

  // 点击切换导航的回调
  changeNav(event) {
    let pageNum = event.currentTarget.dataset.page;
    let nowWeek = pageNum + 1;
    let nowDay = this.getDayOfWeek(nowWeek);
    let month = this.getMonth((nowWeek - 1) * 7);
    this.setData({
      pageNum,
      nowWeek,
      nowDay,
      month,
      monthNum: month / 1, // 当前月份数字类型，用于数字运算
    });
    this.setweekcourse(nowWeek);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    this.setData({
      todayDay: new Date().getDate(),
      todayMonth: new Date().getMonth() + 1,
      day: new Date().getDate(),
      month: new Date().getMonth() + 1,
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {},

  test() {                //测试
    console.log(localdata);
    // console.log(localdata.localdata[0].periods);
    // console.log(typeof(localdata.localdata[0].periods));
    // function pa(periods){
    //   console.log(periods);
    //   console.log(typeof(periods));
    // };
    // pa(localdata.localdata[0].periods);
    this.parseperiods(localdata.localdata[0].periods);
    this.setweekcourse(1);
  },

});
# kb_selenium.py
from selenium import webdriver
from selenium.webdriver.common.by import By
# import time
import requests


# 教务系统
class Jwxt(object):
    def __init__(self):
        # 教务系统登录url
        # self.url = 'https://jxfw.gdut.edu.cn/login!welcome.action'
        self.url = 'https://authserver.gdut.edu.cn/authserver/login?service=http%3A%2F%2Fjxfw.gdut.edu.cn%2Fnew%2FssoLogin'
        # 如果没有将webdriver放到环境变量中，需要执行以下操作
        # webdriver.Chrome(executable_path='./chromdriver')  # 括号中指定文件的路径
        self.driver = None
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36',
            'Cookie': '',
            'Referer': 'https://jxfw.gdut.edu.cn/xsgrkbcx!xskbList.action?xnxqdm=202202&zc=9',
            'Host': 'jxfw.gdut.edu.cn'
        }
        self.kb_url = 'https://jxfw.gdut.edu.cn/xsgrkbcx!getKbRq.action?xnxqdm=202202&zc='

    # 配置driver
    def set_driver(self):
        # 创建配置对象
        options = webdriver.ChromeOptions()
        # 添加配置参数
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=options)

    # 账号密码接口
    def input_info(self):
        # 账号密码接口
        username = input('请输入学号：')
        password = input('请输入密码：')
        print('正在登陆中，请稍候！')
        self.driver.get(self.url)
        # 输入账号密码
        self.driver.find_element(By.XPATH, '//*[@id="username"]').clear()
        self.driver.find_element(By.XPATH, '//*[@id="username"]').send_keys(username)
        self.driver.find_element(By.XPATH, '//*[@id="password"]').clear()
        self.driver.find_element(By.XPATH, '//*[@id="password"]').send_keys(password)
        # 7天免登录
        self.driver.find_element(By.XPATH, '//*[@id="rememberMe"]').click()


    # 登录并返回cookie
    def login(self):
        self.driver.find_element(By.XPATH, '//*[@id="login_submit"]').click()
        # self.driver.find_element(By.XPATH, '//*[@id="submit_btn"]').click()
        # 切换到当前标签
        self.driver.window_handles[-1]

        # 检查是否登陆成功
        now_url = self.driver.current_url
        # print(now_url)
        if now_url != 'https://jxfw.gdut.edu.cn/login!welcome.action':
            return False

        # 返回登录cookie
        cookie = self.driver.get_cookies()[0]
        # print(cookie)
        # 更新请求头的cookie，用作登录课表网页
        self.headers['Cookie'] = 'JSESSIONID=' + cookie['value']
        # print(self.headers)
        return True

    # 课表查询收集
    def select_kb(self):
        # 用列表存储所有课表，数据库存储接口
        all_kb = []
        # 收集全年的数据
        for i in range(1, 23, 1):
            response = requests.get(url=self.kb_url + str(i), headers=self.headers).content.decode()
            kb_list = eval(response)[0]
            # print(kb_list)
            kb_len = len(kb_list)
            # 忽略无课周次
            if kb_len == 0:
                continue

                # 搜集每周的数据，节次、教室、课程、星期、周次、时长
            for j in range(kb_len):
                temp = {'节次': kb_list[j]['jcdm2'], '教室': kb_list[j]['jxcdmc'], '课程': kb_list[j]['kcmc'],
                        '星期': kb_list[j]['xq'], '周次': kb_list[j]['zc'], '时长': kb_list[j]['xs']}
                kb_list[j] = temp
            all_kb.append(kb_list)
        # print(all_kb)
        import json

def write_to_file(all_kb):
    with open('course.txt', 'w') as file:
        # json.dump(all_kb, file, ensure_ascii=False, indent=4)

# 假设您已经计算并获得了all_kb列表
# ...

# write_to_file(all_kb)
        return all_kb

    # 运行
    def run(self):
        self.set_driver()
        while True:
            self.input_info()
            # self.input_verify()
            state = self.login()
            if state is True:
                break
            else:
                print('账号或密码错误，重新输入!')
        # 数据库接入接口
        kb_all = self.select_kb()
        print(kb_all)


if __name__ == '__main__':
    jw = Jwxt()
    jw.run()
    # time.sleep(1)
    jw.driver.quit()

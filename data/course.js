var course =
[{
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-107",
    "course": "计算机网络",
    "day": "7",
    "week": "1",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-105",
    "course": "软件工程",
    "day": "6",
    "week": "1",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02,03,04",
    "where": "教5-207",
    "course": "数据库系统",
    "day": "6",
    "week": "1",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-512",
    "course": "现代食品发酵技术",
    "day": "5",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "4",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-218",
    "course": "食品机械与设备",
    "day": "3",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "05",
    "where": "教1-318",
    "course": "工程管理",
    "day": "3",
    "week": "1",
    "duration": "1"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-429",
    "course": "食品添加剂与应用",
    "day": "1",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-429",
    "course": "食品加工新技术",
    "day": "1",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "1",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-107",
    "course": "计算机网络",
    "day": "7",
    "week": "2",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-105",
    "course": "软件工程",
    "day": "6",
    "week": "2",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02,03,04",
    "where": "教5-207",
    "course": "数据库系统",
    "day": "6",
    "week": "2",
    
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-512",
    "course": "现代食品发酵技术",
    "day": "5",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-512",
    "course": "现代食品仪器分析",
    "day": "5",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-429",
    "course": "食品添加剂与应用",
    "day": "1",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-429",
    "course": "食品加工新技术",
    "day": "1",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会义理论体系概论",
    "day": "4",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-218",
    "course": "食品机械与设备",
    "day": "3",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "2",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-429",
    "course": "食品添加剂与应用",
    "day": "1",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-429",
    "course": "食品加工新技术",
    "day": "1",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-107",
    "course": "计算机网络",
    "day": "7",
    "week": "3",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-105",
    "course": "软件工程",
    "day": "6",
    "week": "3",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02,03,04",
    "where": "教5-207",
    "course": "数据库系统",
    "day": "6",
    "week": "3",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-512",
    "course": "现代食品发酵技术",
    "day": "5",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-512",
    "course": "现代食品仪器分析",
    "day": "5",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "4",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-218",
    "course": "食品机械与设备",
    "day": "3",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "3",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-105",
    "course": "软件工程",
    "day": "6",
    "week": "4",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02,03,04",
    
    "where": "教5-207",
    "course": "数据库系统",
    "day": "6",
    "week": "4",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-512",
    "course": "现代食品发酵技术",
    "day": "5",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-512",
    "course": "现代食品仪器分析",
    "day": "5",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "4",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-107",
    "course": "计算机网络",
    "day": "7",
    "week": "4",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02",
    
    "where": "教2-218",
    "course": "食品机械与设备",
    "day": "3",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-429",
    "course": "食品添加剂与应用",
    "day": "1",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-429",
    "course": "食品加工新技术",
    "day": "1",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "4",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-105",
    "course": "软件工程",
    "day": "6",
    "week": "5",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02,03,04",
    "where": "教5-207",
    "course": "数据库系统",
    "day": "6",
    "week": "5",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-519",
    "course": "食品工艺学",
    "day": "5",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    
    "day": "4",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-218",
    "course": "食品机械与设备",
    "day": "3",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "5",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-519",
    "course": "食品工艺学",
    "day": "5",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "4",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02,03,04",
    "where": "教5-107",
    "course": "计算机网络",
    "day": "7",
    "week": "6",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-218",
    "course": "食品机械与设备",
    "day": "3",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "3号大教室",
    "course": "形势与政策",
    "day": "2",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "6",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "3号大教室",
    "course": "形势与政策",
    "day": "2",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-107",
    "course": "计算机网络",
    "day": "7",
    "week": "7",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-105",
    "course": "软件工程",
    "day": "6",
    "week": "7",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02,03,04",
    "where": "教5-207",
    "course": "数据库系统",
    "day": "6",
    "week": "7",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-519",
    "course": "食品工艺学",
    "day": "5",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-218",
    "course": "食品机械与设备",
    "day": "3",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "7",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教3-109",
    "course": "劳动教育",
    "day": "1",
    "week": "8",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "8",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "8",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "8",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "8",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "8",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "8",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "8",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "8",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-519",
    "course": "食品工艺学",
    "day": "5",
    "week": "8",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-105",
    "course": "软件工程",
    "day": "6",
    "week": "8",
    "duration": "4"
    },

    {
    "id": "1",
    "periods": "01,02,03,04",
    "where": "教5-207",
    "course": "数据库系统",
    "day": "6",
    "week": "8",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-107",
    "course": "计算机网络",
    "day": "7",
    "week": "8",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-107",
    "course": "计算机网络",
    "day": "7",
    "week": "9",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-105",
    "course": "软件工程",
    "day": "6",
    "week": "9",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02,03,04",
    "where": "教5-207",
    "course": "数据库系统",
    "day": "6",
    "week": "9",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-519",
    "course": "食品工艺学",
    "day": "5",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教3-110",
    "course": "大学生就业创业指导",
    "day": "4",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "9",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-519",
    "course": "食品工艺学",
    "day": "5",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教3-110",
    "course": "大学生就业创业指导",
    "day": "4",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "10",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-107",
    "course": "计算机网络",
    "day": "7",
    "week": "11",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "3号大教室",
    "course": "形势与政策",
    "day": "5",
    "week": "11",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-519",
    "course": "食品工艺学",
    "day": "5",
    "week": "11",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "11",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "11",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教3-110",
    "course": "大学生就业创业指导",
    "day": "4",
    "week": "11",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "11",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "11",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "11",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "11",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "11",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-105",
    "course": "软件工程",
    "day": "6",
    "week": "12",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02,03,04",
    "where": "教5-207",
    "course": "数据库系统",
    "day": "6",
    "week": "12",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-518",
    "course": "食品添加剂与应用",
    "day": "4",
    "week": "12",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教2-518",
    "course": "食品加工新技术",
    "day": "4",
    "week": "12",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教3-110",
    "course": "大学生就业创业指导",
    "day": "4",
    "week": "12",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "12",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "12",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-107",
    "course": "计算机网络",
    "day": "7",
    "week": "12",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "08,09",
    "where": "教1-424",
    "course": "现代食品发酵技术",
    "day": "3",
    "week": "12",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "12",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "12",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "12",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "12",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "06,07,08,09",
    "where": "教5-105",
    "course": "软件工程",
    "day": "6",
    "week": "13",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "01,02,03,04",
    "where": "教5-207",
    "course": "数据库系统",
    "day": "6",
    "week": "13",
    "duration": "4"
    },
    {
    "id": "1",
    "periods": "06,07",
    "where": "教1-424",
    "course": "现代食品仪器分析",
    "day": "3",
    "week": "13",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-105",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "3",
    "week": "13",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-419",
    "course": "食品机械与设备",
    "day": "1",
    "week": "13",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教3-110",
    "course": "习近平新时代中国特色社会主义思想概论",
    "day": "1",
    "week": "13",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "03,04",
    "where": "教2-315",
    "course": "食品工艺学",
    "day": "2",
    "week": "13",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-314",
    "course": "毛泽东思想和中国特色社会主义理论体系概论",
    "day": "2",
    "week": "13",
    "duration": "2"
    },
    {
    "id": "1",
    "periods": "01,02",
    "where": "教1-429",
    "course": "食品机械与设备课程设计",
    "day": "1",
    "week": "18",
    "duration": "2"
    }]
    function autoIncrementIds(coursesArray) {
        let currentId = 1; // 初始化ID计数器
        coursesArray.forEach((course) => {
            course.id = currentId.toString(); // 更新当前对象的id值
            currentId++; // 增加ID计数器
        });
        return coursesArray; // 返回处理后的数组
    }
    // 应用函数，自动编号id字段
    const coursesWithAutoIds = autoIncrementIds(course);

    module.exports = {
        localdata : course
    }